import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ChillerService} from '../shared/services/chiller.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  chillerFilterForm: FormGroup;
  reports;
  selectedReport;
  selectedDay;
  weeks = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  paginate = {
    total: 0,
    perpage: 10,
    page: 1
  };
  closeResult: string;

  constructor(private router: Router,
              private chillerService: ChillerService,
              private fb: FormBuilder,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.chillerFilterForm = this.fb.group({
      fromDate: [null],
      toDate: [null]
    });
    this.getReports(this.paginate);
  }

  getReports(params) {
    this.chillerService.report(params).subscribe(
      data => this.mapReport(data.data)
    );
  }

  mapReport(data) {
    console.log('data', data);
    console.log(data);
    this.reports = [];
    data.forEach(report => {
      report['purityScore'] = 0;
      this.weeks.forEach(week => {
        if (report[week] && report[week].purity) {
          report['purityScore']++;
        }
      });
      this.reports.push(report);
    });
  }

  navigateToDetails(chillerId) {
    this.router.navigateByUrl('chiller/' + chillerId + '/details');
  }

  search() {
    const reqData = this.chillerFilterForm.value;
    const newReqData = {};

    if (reqData['fromDate']) {
      newReqData['fromDate'] = moment(reqData['fromDate']['year'] + '-' + reqData['fromDate']['month'] + '-' + reqData['fromDate']['day']).format('YYYY-MM-DD');
    }
    if (reqData['toDate']) {
      newReqData['toDate'] = moment(reqData['toDate']['year'] + '-' + reqData['toDate']['month'] + '-' + reqData['toDate']['day']).format('YYYY-MM-DD');
    }

    this.getReports(newReqData);
  }

  open(content, report, index) {
    this.selectedReport = report;
    this.selectedDay = this.weeks[index];
    this.modalService.open(content, {backdropClass: 'modal-dialog-full', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
