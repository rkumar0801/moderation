import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReportRoutingModule} from './report-routing.module';
import {ReportComponent} from './report.component';
import {ChillerService} from '../shared/services/chiller.service';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ReportComponent],
  providers: [ChillerService]
})

export class ReportModule {
}
