import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'chiller',
    name: 'Chiller',
    type: 'link',
    icon: 'basic-accelerator'
  },
  {
    state: 'report',
    name: 'Monthly Report',
    type: 'link',
    icon: 'basic-accelerator'
  },
  /*{
    state: 'users',
    name: 'Users',
    type: 'sub',
    icon: 'basic-accelerator',
    children: [
      {
        state: 'list',
        name: 'View Users'
      },
      {
        state: 'add',
        name: 'Add Users'
      }
    ]
  },*/
  /*{
    state: 'authentication',
    name: 'AUTHENTICATION',
    type: 'sub',
    icon: 'basic-lock-open',
    children: [
      {
        state: 'signin',
        name: 'SIGNIN'
      },
      {
        state: 'signup',
        name: 'SIGNUP'
      },
      {
        state: 'forgot',
        name: 'FORGOT'
      },
      {
        state: 'lockscreen',
        name: 'LOCKSCREEN'
      },
    ]
  },
  {
    state: 'docs',
    name: 'DOCS',
    type: 'link',
    icon: 'basic-sheet-txt'
  }*/
];

@Injectable()
export class MenuService {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
