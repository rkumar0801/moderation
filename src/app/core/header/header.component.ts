import {Component, EventEmitter, Output, Input, OnInit} from '@angular/core';
import {StorageService} from '../../shared/services/storage.service';
import {Router} from '@angular/router';
import {GlobalService} from '../../shared/services/global.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() heading: string;
  @Output() toggleSidebar = new EventEmitter<void>();
  @Output() openSearch = new EventEmitter<void>();
  @Output() toggleFullscreen = new EventEmitter<void>();
  user;
  role;

  constructor(private router: Router,
              private globalService: GlobalService) {

  }

  ngOnInit() {
    this.globalService.user.subscribe(
      value => this.user = value
    );
    this.globalService.userRole.subscribe(
      value => this.role = value
    );
  }

  logout() {
    StorageService.clearAll();
    this.router.navigateByUrl('signin');
  }
}
