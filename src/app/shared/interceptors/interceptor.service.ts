import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageService} from '../services/storage.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const headers = {'Content-Type': 'application/json'};

    // todo add token from shared service
    if (StorageService.getItem('accessToken')) {
      headers['Authorization'] = 'Bearer ' + StorageService.getItem('accessToken');
    }

    const dupReq = req.clone({
      setHeaders: headers
    });

    return next.handle(dupReq);
  }
}

export const HtpInterceptor = [{provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}];
