import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {AppUrl} from '../constants/app-url';

@Injectable()
export class AuthService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }

  login(data): Observable<any> {
    return this.http.post(AppUrl.LOG_IN, data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  list(): Observable<any> {
    return this.http.get('http://api.rms.luezoid.com/api/v1/invoices')
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  registration(data): Observable<any> {
    return this.http.post(AppUrl.USERS(), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

}
