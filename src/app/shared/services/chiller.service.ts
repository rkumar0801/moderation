import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {AppUrl} from '../constants/app-url';
import {BaseService} from './base.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class ChillerService {

  constructor(private http: HttpClient) {
  }

  list(params): Observable<any> {
    return this.http.get(AppUrl.CHILLERS(), {params: params})
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  update(Id, data): Observable<any> {
    return this.http.put(AppUrl.CHILLERS(Id), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  destroy(Id): Observable<any> {
    return this.http.delete(AppUrl.CHILLERS(Id))
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  details(Id): Observable<any> {
    return this.http.get(AppUrl.CHILLER_LIST(Id))
    .pipe(
      catchError(BaseService.handleError)
    );
  }

  appove(chillerId, data): Observable<any> {
    return this.http.put(AppUrl.CHILLER_APPROVE(chillerId), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  reject(chillerId, data): Observable<any> {
    return this.http.put(AppUrl.CHILLER_REJECT(chillerId), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  chillerCount(): Observable<any> {
    return this.http.get(AppUrl.CHILLER_COUNT())
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  submit(chillerId, data): Observable<any> {
    return this.http.put(AppUrl.CHILLER_SUBMIT(chillerId), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  report(params): Observable<any> {
    return this.http.get(AppUrl.CHILLER_REPORT(), {params: params})
      .pipe(
        catchError(BaseService.handleError)
      );
  }
}
