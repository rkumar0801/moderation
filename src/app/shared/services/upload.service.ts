import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';
import {Observable} from 'rxjs';

@Injectable()
export class UploadService {
  progress$;
  progressObserver;
  progress;

  constructor() {
    this.progress$ = Observable.create(observer => {
      this.progressObserver = observer;
    }).share();
  }

  makeFileRequest(url: string, params: any, files, filesKey?, isMultiple?): Observable<any> {
    return Observable.create(observer => {
      const formData: FormData = new FormData();
      const xhr: XMLHttpRequest = new XMLHttpRequest();

      filesKey = filesKey ? filesKey : 'file';

      for (const param in params) {
        formData.append(param, params[param]);
      }

      if (isMultiple) {
        for (let i = 0; i < files.length; i++) {
          formData.append(filesKey + '[' + i + ']', files[i], files[i].name);
        }
      } else {
        formData.append(filesKey, files[0], files[0].name);
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            observer.error(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round(event.loaded / event.total * 100);
        // this.progressObserver.next(this.progress);
      };

      xhr.open('POST', url, true);
      if (StorageService.getItem('access_token')) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + StorageService.getItem('access_token'));
      }
      xhr.send(formData);
    });
  }

}
