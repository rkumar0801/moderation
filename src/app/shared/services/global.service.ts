import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable()

export class GlobalService {
  public user: BehaviorSubject<string> = new BehaviorSubject(null);
  public accessToken: BehaviorSubject<string> = new BehaviorSubject(null);
  public userRole: BehaviorSubject<string> = new BehaviorSubject(null);


  constructor() {
    if (StorageService.getItem('accessToken')) {
      this.accessToken.next(StorageService.getItem('accessToken'));
    }
    if (StorageService.getItem('user')) {
      this.user.next(StorageService.getItem('user'));
    }
    if (StorageService.getItem('role')) {
      this.userRole.next(StorageService.getItem('role'));
    }
  }

  setAccessToken(token) {
    this.accessToken.next(token);
  }

  setUser(user) {
    this.user.next(user);
  }

  setUserRole(role) {
    this.userRole.next(role);
  }

}
