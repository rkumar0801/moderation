import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UploadService} from './upload.service';
import {Observable} from 'rxjs/Observable';
import {AppUrl} from '../constants/app-url';
import {BaseService} from './base.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  list(params): Observable<any> {
    console.log('in service');
    return this.http.get(AppUrl.USERS(), {params: params})
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  create(data): Observable<any> {
    return this.http.post(AppUrl.USERS(), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  details(userId): Observable<any> {
    return this.http.get(AppUrl.USERS(userId))
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  update(userId, data): Observable<any> {
    return this.http.put(AppUrl.USERS(userId), data)
      .pipe(
        catchError(BaseService.handleError)
      );
  }

  destroy(userId): Observable<any> {
    return this.http.delete(AppUrl.USERS(userId))
      .pipe(
        catchError(BaseService.handleError)
      );
  }
}
