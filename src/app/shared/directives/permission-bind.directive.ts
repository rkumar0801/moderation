import {Directive, ElementRef, Input} from '@angular/core';
import {GlobalService} from '../services/global.service';

// import {Subscription} from 'rxjs';

@Directive({selector: '[appPermissionBind]'})
export class PermissionBindDirective {
  permission: string;
  role: string;
  onlyAdmin: boolean;
  onlyClient: boolean;
  envTypes: any;
  userRole: any;

  @Input('role')
  set setRole(value: string) {
    this.role = value;
    this.checkVisibility();
  }

  @Input('permission')
  set setPermission(value: string) {
    this.permission = value;
    this.verifyPermission();
  }

  @Input('onlyAdmin')
  set setOnlyAdmin(value: boolean) {
    this.onlyAdmin = value;
    this.verifyPermission();
  }

  @Input('onlyClient')
  set setOnlyClient(value: boolean) {
    this.onlyClient = value;
    this.verifyPermission();
  }

  @Input('envTypes')
  set setEnvTypes(value: any) {
    this.envTypes = value;
    this.verifyPermission();
  }

  // envChange: Subscription;

  constructor(private el: ElementRef, private globalService: GlobalService) {
    /*this.envChange =
      this.globalService.currentEnv$
        .subscribe(
          (currentEnv) => {
            this.verifyPermission();
          }
        );*/
    /*this.globalService.permissions.subscribe(
      value => this.verifyPermission()
    );*/
    this.globalService.user.subscribe(
      value => {
        this.userRole = value['type'];
        this.checkVisibility();
      }
    );
  }

  checkVisibility() {
    if (this.role !== this.userRole) {
      this.el.nativeElement.style.display = 'none';
    } else {
      this.el.nativeElement.style.display = 'block';
    }
  }

  verifyPermission() {
    /*const permissions = this.globalService.permissions.value;
    const isSuperAdmin = this.globalService.userRoles.value.indexOf('Super Admin') !== -1;
// console.log('-----------', this.globalService.userRoles.value, isSuperAdmin)
    if (!isSuperAdmin && this.permission) {
      let found = false;
      // console.log(this.permission, permissions.length);
      for (let i = 0; i < permissions.length; i++) {
        // console.log(this.permission, permissions[i]['action']);
        if (this.permission === permissions[i]['action']) {
          found = true;
          break;
        }
      }
      if (found) {
        this.el.nativeElement.style.display = 'block';
      } else {
        this.el.nativeElement.style.display = 'none';
      }
    } else if (isSuperAdmin) {
      this.el.nativeElement.style.display = 'block';
    }*/

    /*let isNone = false;
    if (this.permission) {
      let found = false;
      if (this.permission && this.globalService.getCurrentEnv() &&
        this.globalService.getCurrentEnv().permissions &&
        this.globalService.getCurrentEnv().permissions.length) {
        const permissions = this.globalService.getCurrentEnv().permissions;
        permissions.forEach((permission) => {
          const permissionName = permission.entityName + '.' + permission.name;
          if (this.permission === permissionName) {
            found = true;
            return;
          }
        });
      }
      if (!found) {
        isNone = true;
        // this.el.nativeElement.style.display = 'none';
      }
    }
    if (this.onlyAdmin && this.globalService.company.id !== 1) {
      isNone = true;
    }
    if (this.onlyClient && this.globalService.company.id === 1) {
      isNone = true;
    }
    if (this.envTypes && this.envTypes.length && this.envTypes.indexOf(this.globalService.getCurrentEnv().type) === -1) {
      isNone = true;
    }
    if (isNone) {
      this.el.nativeElement.style.display = 'none';
    } else {
      this.el.nativeElement.style.display = 'inherit';
    }*/
  }
}
