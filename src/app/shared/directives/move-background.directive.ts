import {Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appMoveBackground]'
})
export class MoveBackgroundDirective implements OnInit, OnChanges {

  el: HTMLImageElement;
  backgroundSrc: string;
  xPos: number;
  yPos: number;

  @Input('backgroundSrc')
  set setBackgroundSrc(value: string) {
    this.backgroundSrc = value;
  }

  @Input('xPos')
  set setXPos(value: number) {
    this.xPos = value;
  }

  @Input('yPos')
  set setYPos(value: number) {
    this.yPos = value;
  }

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }


  ngOnInit() {
    this.el.style.backgroundImage = this.buildBackground(this.backgroundSrc);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.xPos && this.yPos && this.backgroundSrc) {
      this.el.style.backgroundPosition = this.buildPosition(this.xPos, this.yPos)
    }
  }

  buildPosition(xPos: number, yPos: number): string {
    return xPos + '% ' + yPos + '%';
  }

  buildBackground(src: string): string {
    return 'url(' + src + ')';
  }

}
