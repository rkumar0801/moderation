import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PermissionBindDirective} from './permission-bind.directive';
import { MoveBackgroundDirective } from './move-background.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PermissionBindDirective, MoveBackgroundDirective],
  exports: [PermissionBindDirective, MoveBackgroundDirective]
})
export class DirectivesModule { }
