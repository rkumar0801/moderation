import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable()
export class AppUrl {

  constructor() {
  }

  public static get APP_URL_V1(): string {
    return environment.appUrl + 'admin/';
  }

  public static get LOG_IN(): string {
    return AppUrl.APP_URL_V1 + 'authenticate';
  }

  public static get FILE_UPLOAD(): string {
    return AppUrl.APP_URL_V1 + 'files';
  }

  public static USERS(userId?: string): string {
    const url = userId ? '/' + userId : '';
    return AppUrl.APP_URL_V1 + 'users' + url;
  }

  public static CHILLERS(chillerId?: string): string {
    const url = chillerId ? '/' + chillerId : '';
    return AppUrl.APP_URL_V1 + 'chiller-list' + url;
  }

  public static CHILLER_LIST(chillerId?: string): string {
    const url = chillerId ? '/' + chillerId : '';
    return AppUrl.APP_URL_V1 + 'chiller-list' + url;
  }

  public static CHILLER_SUBMIT(chillerId: string): string {
    return AppUrl.APP_URL_V1 + 'chillers/' + chillerId + '/submit';
  }

  public static CHILLER_APPROVE(chillerId: string): string {
    return AppUrl.APP_URL_V1 + 'chillers/' + chillerId + '/approve';
  }

  public static CHILLER_REJECT(chillerId: string): string {
    return AppUrl.APP_URL_V1 + 'chillers/' + chillerId + '/reject';
  }

  public static CHILLER_COUNT(): string {
    return AppUrl.APP_URL_V1 + 'chiller-count';
  }

  public static CHILLER_REPORT(): string {
    return AppUrl.APP_URL_V1 + 'chiller-report';
  }

}
