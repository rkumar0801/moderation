import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './core';
import {AuthLayoutComponent} from './core';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from './shared/guards/auth-guard.service';

export const AppRoutes: Routes = [
  /*{
    path: '',
    component: DashboardComponent
  },*/
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: '/chiller',
        pathMatch: 'full'
      },
      {
        path: 'chiller',
        loadChildren: './chiller/chiller.module#ChillerModule'
      },
      {
        path: 'report',
        loadChildren: './report/report.module#ReportModule'
      },
      {
        path: 'docs',
        loadChildren: './docs/docs.module#DocsModule'
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [{
      path: '',
      loadChildren: './authentication/authentication.module#AuthenticationModule'
    }, {
      path: 'error',
      loadChildren: './error/error.module#ErrorModule'
    }]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];

