import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';
import {GlobalService} from '../../shared/services/global.service';
import {StorageService} from '../../shared/services/storage.service';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public form: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private toasterService: ToasterService,
              private globalService: GlobalService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      username: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });

    // if user is already logged in navigate to dashboard
    if (StorageService.getItem('accessToken')) {
      this.router.navigateByUrl('chiller');
      // return this.router.navigate(['/']);
    }
  }

  login() {
    this.authService.login(this.form.value).subscribe(
      data => this.afterLogin(data),
      err => this.showError(err)
    );
  }

  afterLogin(data) {
    const accessToken = data.data.token;
    const user = data.data.user;
    const role = data.data.role[1];

    this.globalService.setAccessToken(accessToken);
    this.globalService.setUser(user);
    this.globalService.setUserRole(role);

    StorageService.setItem('user', user);
    StorageService.setItem('role', role);
    StorageService.setItem('accessToken', accessToken);

    this.router.navigateByUrl('chiller');
    // this.router.navigate(['/']);
  }

  showError(err) {
    console.log('err', err);
    this.showToast(err, 'error');
  }

  showToast(body, type?) {
    let title = 'Error';
    if (type === 'success') {
      title = 'Success';
    } else {
      type = 'error';
      title = 'Error';
    }
    this.toasterService.pop(type, title, body);
  }

}
