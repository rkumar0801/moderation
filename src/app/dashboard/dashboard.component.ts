import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {UploadService} from '../shared/services/upload.service';
import {Router} from '@angular/router';
import {StorageService} from '../shared/services/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  invoices: Array<any> = [];
  options = [
    {title: 't'},
    {title: 't'},
    {title: 't'},
    {title: 't'},
    {title: 't'},
    {title: 't'}
  ];

  constructor(private uploadService: UploadService, private router: Router) {
    if (!StorageService.getItem('accessToken')) {
      this.router.navigateByUrl('signin');
      // return this.router.navigate(['/']);
    } else {
      this.router.navigateByUrl('chiller');
    }

  }

  ngOnInit() {
  }

  onChange(event: Event) {
    const element: HTMLInputElement = <HTMLInputElement>event.srcElement;
    const files = element.files;
    this.uploadService.makeFileRequest('http://penpencil.xyz:2232/v1/files/upload-image', {}, files, 'image').subscribe(
      data => console.log('---->file data', data),
      error => console.log(error)
    );
  }


}
