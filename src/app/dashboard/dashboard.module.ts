import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutes} from './dashboard.routing';
import {AuthService} from '../shared/services/auth.service';
import {UploadService} from '../shared/services/upload.service';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(DashboardRoutes)],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
  providers: [UploadService]
})

export class DashboardModule {
}
