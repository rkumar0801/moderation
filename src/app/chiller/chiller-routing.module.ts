import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ChillerComponent} from './chiller.component';
import {ChillerDetailComponent} from './chiller-detail/chiller-detail.component';

const routes: Routes = [
  {
    path: '', component: ChillerComponent, data: {heading: 'Chiller'}
  },
  {
    path: ':chillerId/details', component: ChillerDetailComponent, data: {heading: 'Update Chiller'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChillerRoutingModule {
}
