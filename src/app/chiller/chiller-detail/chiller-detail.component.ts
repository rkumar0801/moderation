import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ChillerService} from '../../shared/services/chiller.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '../../shared/services/global.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-chiller-detail',
  templateUrl: './chiller-detail.component.html',
  styleUrls: ['./chiller-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChillerDetailComponent implements OnInit {
  chillerForm: FormGroup;
  chiller;
  chillerCount;
  closeResult: string;
  dialogStatus;
  modalRef;

  constructor(private fb: FormBuilder,
              private router: Router,
              private globalService: GlobalService,
              private activatedRoute: ActivatedRoute,
              private chillerService: ChillerService,
              private toasterService: ToasterService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.chillerForm = this.fb.group({
      id: [null],
      name: [null],
      imageName: [null],
      day: [null],
      msisdn: [null],
      state: [null],
      clarity: new FormControl(),
      imageSize: new FormControl(),
      purity: new FormControl(),
      moderatorRemark: [null],
      adminRemark: [null],
      createdAt: [null]
    });

    this.globalService.user.subscribe(
      value => {
        if (value['role'] && value['role']['name'] === 'Admin') {
          this.chillerForm.get('moderatorRemark').disable();
        }
      }
    );

    this.getChillerCount();

    const chillerId = this.activatedRoute.snapshot.paramMap.get('chillerId');

    if (chillerId) {
      this.getChillerDetails(chillerId);
    }
  }

  getChillerDetails(chillerId) {
    this.chillerService.details(chillerId).subscribe(
      data => this.mapChillers(data.data)
    );
  }

  getChillerCount() {
    this.chillerService.chillerCount().subscribe(
      data => this.chillerCount = data.data
    );
  }

  mapChillers(data) {
    this.chiller = data;

    this.chillerForm.patchValue({
      id: data.id,
      name: data.user.cSMNAME,
      imageName: data.file.name,
      day: data.status === 'APPROVED' ? 1 : 0,
      msisdn: data.user.msisdn,
      state: data.user.state,
      clarity: data.clarity,
      imageSize: data.imageSize,
      purity: data.purity,
      moderatorRemark: data.moderatorRemark,
      adminRemark: data.adminRemark,
      createdAt: data.createdAt
    });
  }

  submitDetails() {
    const reqObj = this.chillerForm.value;
    const deleteKeys = ['name', 'imageId', 'day', 'createdAt', 'imageName'];

    for (let i = 0; i < deleteKeys.length; i++) {
      delete reqObj[deleteKeys[i]];
    }

    this.chillerService.submit(reqObj['id'], reqObj).subscribe(
      data => this.afterSubmit(data.data)
    );
  }

  submitStatusDetails(status) {
    const reqObj = this.chillerForm.value;
    const deleteKeys = ['name', 'imageId', 'day', 'createdAt', 'imageName'];

    for (let i = 0; i < deleteKeys.length; i++) {
      delete reqObj[deleteKeys[i]];
    }

    if (status === 'APPROVE') {
      this.chillerService.appove(reqObj['id'], reqObj).subscribe(
        data => this.afterSubmit(data.data),
        error => this.showError(error)
      );
    } else {
      this.chillerService.reject(reqObj['id'], reqObj).subscribe(
        data => this.afterSubmit(data.data),
        error => this.showError(error)
      );
    }
  }

  afterSubmit(data) {
    if (this.modalRef) {
      this.modalRef.close();
    }
    this.router.navigateByUrl('chiller');
  }

  open(content) {
    this.modalService.open(content, {
      windowClass: 'modal-xxl',
      backdropClass: 'modal-dialog-full',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openConfirmation(content, status) {
    this.dialogStatus = status;
    this.modalRef = this.modalService.open(content, {backdropClass: 'modal-dialog-full', centered: true});
  }

  showError(err) {
    if (this.modalRef) {
      this.modalRef.close();
    }
    this.showToast(err, 'error');
  }

  showToast(body, type?) {
    let title = 'Error';
    if (type === 'success') {
      title = 'Success';
    } else {
      type = 'error';
      title = 'Error';
    }
    this.toasterService.pop(type, title, body);
  }

}
