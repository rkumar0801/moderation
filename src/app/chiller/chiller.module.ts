import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ChillerRoutingModule} from './chiller-routing.module';
import {ChillerComponent} from './chiller.component';
import {ChillerDetailComponent} from './chiller-detail/chiller-detail.component';
import {ChillerService} from '../shared/services/chiller.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import { ImageZoomComponent } from './image-zoom/image-zoom.component';
import {DirectivesModule} from '../shared/directives/directives.module';
import {NgxImageZoomModule} from 'ngx-image-zoom';
import {ImageZoomModule} from 'angular2-image-zoom';
import {ToasterModule} from 'angular2-toaster';

@NgModule({
  imports: [
    CommonModule,
    ChillerRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    DirectivesModule,
    NgxImageZoomModule.forRoot(),
    ImageZoomModule,
    ToasterModule.forRoot()
  ],
  declarations: [
    ChillerComponent,
    ChillerDetailComponent,
    ImageZoomComponent
  ],
  providers: [
    ChillerService
  ]
})
export class ChillerModule {
}
