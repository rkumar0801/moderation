import {Component, OnInit} from '@angular/core';
import {ChillerService} from '../shared/services/chiller.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-chiller',
  templateUrl: './chiller.component.html',
  styleUrls: ['./chiller.component.scss']
})
export class ChillerComponent implements OnInit {
  chillerFilterForm: FormGroup;

  paginate = {
    total: 0,
    perpage: 10,
    page: 1
  };
  filters = {};
  chillers = [];

  constructor(private chillerService: ChillerService,
              private router: Router,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.chillerFilterForm = this.fb.group({
      fromDate: [null],
      toDate: [null],
      status: ['']
    });
    this.getChillers(this.paginate);
  }

  getChillers(params) {
    this.chillerService.list(params).subscribe(
      data => this.mapChillers(data)
    );
  }

  mapChillers(data) {
    this.chillers = data.data.items;
    this.paginate = {total: data.data.total, perpage: data.data.perpage, page: data.data.page};
  }

  applyPagination(event) {
    this.getChillers(Object.assign(this.filters, this.paginate));
  }

  navigateToDetails(chiller) {
    this.router.navigateByUrl('chiller/' + chiller.id + '/details');
  }

  search() {
    console.log('==>', this.chillerFilterForm.value);
    const reqData = this.chillerFilterForm.value;

    if (reqData['fromDate']) {
      this.filters['fromDate'] = moment(reqData['fromDate']['year'] + '-' + reqData['fromDate']['month'] + '-' + reqData['fromDate']['day']).format('YYYY-MM-DD');
    }
    if (reqData['toDate']) {
      this.filters['toDate'] = moment(reqData['toDate']['year'] + '-' + reqData['toDate']['month'] + '-' + reqData['toDate']['day']).format('YYYY-MM-DD');
    }
    if (reqData['status'] !== '') {
      this.filters['status'] = reqData['status'];
    }

    this.filters = Object.assign({total: 0,
      perpage: 10,
      page: 1}, this.filters);

    this.getChillers(this.filters);
  }

}
